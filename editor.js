//require(moduleHelper.js)

const MODULE_NAME = "editor";

let editor = document.getElementById("rawEditor");

editor.addEventListener("focusout",e=>emitEvent("fileChange",{fileContent:editor.value},"*"));

cmds = {
    "log" : m => console.log(m.msg),
    "ping" : m => {console.log("ping");messageModule({cmd:"log",msg:"pong"},"editor");},
    "openFile" : openFile,
    "getFile" : getFile
}

function openFile(m) {
    editor.value = m.fileContent;
}

function getFile(m) {
    m.callback.fileContent = editor.value;
    messageModule(m.callback);
}